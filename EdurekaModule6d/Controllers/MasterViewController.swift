//
//  MasterViewController.swift
//  EdurekaModule6d
//
//  Created by Jay on 18/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//we need a protocol to navigate from an item or show the item detail
//this is linked up in the Details View Controller

protocol ItemSelectionDelegate: class
{
//    func monsterSelected(_ newMonster: Monster)
    func itemSelected()
    func itemSelected2(rowNumber:Int)
}

//In our case, the Root View Controller in the Split UI is the Master Controller, since it controls the main display.

class MasterViewController: UITableViewController
{
    //get a reference to the delegate.
    //make sure that this delegate points to the Details View Controller in the app delegate using the master reference
    weak var delegate: ItemSelectionDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //set the number of rows. lets keep it 10
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    //return cells for each row
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        //just add something to show.
        //skip using the model we already did that during the table discussion.
        cell.textLabel?.text = "row number - " + String(indexPath.row)
        return cell
    }
    
    //gets triggered when an element is tapped.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //let selectedMonster = monsters[indexPath.row]
        //delegate?.monsterSelected(selectedMonster)
        //call the detail view method with which you can do something
        print("You tapped element at - " + String(indexPath.row))
        delegate?.itemSelected()
        delegate?.itemSelected2(rowNumber: indexPath.row)
        
        //okay, add the following to make the navigation happen on iPhone.
        let detailViewController = delegate
        splitViewController?.showDetailViewController(detailViewController as! UIViewController, sender: nil)
    }
}
