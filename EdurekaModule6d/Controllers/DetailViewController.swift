//
//  DetailViewController.swift
//  EdurekaModule6d
//
//  Created by Jay on 18/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//this is for the View Controller which we will eventually hook up to show the details 
class DetailViewController: UIViewController
{
    @IBOutlet var nameOfLabel: UILabel!
    var someString: String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setLable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setLable()
    {
        nameOfLabel.text = someString
    }
    
    
}

extension DetailViewController: ItemSelectionDelegate
{
    func itemSelected()
    {
        //monster = newMonster
        print("itemSelected called")
    }
    
    func itemSelected2(rowNumber:Int)
    {
        print("You have selected the row + " + String(rowNumber))
        nameOfLabel.text = "Row" + String(rowNumber)
    }
}
